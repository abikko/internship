package kz.edu.astanait.calendar.main_calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kz.edu.astanait.calendar.R

class CalendarAdapter(val listOfNumbers : ArrayList<Int>) : RecyclerView.Adapter<CalendarAdapter.CalendarAdapterViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarAdapterViewHolder =
        CalendarAdapterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_date_numbers,null))

    override fun onBindViewHolder(holder: CalendarAdapterViewHolder, position: Int) {
        listOfNumbers.get(position)
//        holder.tvNumberDateOne.setText()
        
    }

    override fun getItemCount(): Int = listOfNumbers.size

    class CalendarAdapterViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView){
        var tvNumberDateOne : TextView? = null
        var tvNumberDateTwo : TextView? = null
        var tvNumberDateThree : TextView? = null
        var tvNumberDateFour : TextView? = null
        var tvNumberDateFive : TextView? = null
        var tvNumberDateSix : TextView? = null
        var tvNumberDateSeven : TextView? = null

        init {
            tvNumberDateOne = itemView.findViewById(R.id.tvDateNumbers1)
            tvNumberDateTwo = itemView.findViewById(R.id.tvDateNumbers2)
            tvNumberDateThree = itemView.findViewById(R.id.tvDateNumbers3)
            tvNumberDateFour = itemView.findViewById(R.id.tvDateNumbers4)
            tvNumberDateFive = itemView.findViewById(R.id.tvDateNumbers5)
            tvNumberDateSix = itemView.findViewById(R.id.tvDateNumbers6)
            tvNumberDateSeven = itemView.findViewById(R.id.tvDateNumbers7)
        }
    }

}