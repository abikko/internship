package kz.edu.astanait.calendar.ui.written_events

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.EventDay
import kz.edu.astanait.calendar.R
import kz.edu.astanait.calendar.ui.MainActivityViewModel
import java.util.*
import kotlin.collections.ArrayList

class EventsActivity : AppCompatActivity() {
    private val TAG = "EVENT_ACTIVITY"
    private var eventCalendar: CalendarView? = null

    var lastDate: Calendar? = null
    var firstDate: Calendar? = null
    var choosedColor: Int? = null

    var listOfDate: ArrayList<Calendar>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)
        eventCalendar = findViewById(R.id.cvEvents)
        val intent = intent

        lastDate = intent.getSerializableExtra("calendarLastTime") as Calendar?
        firstDate = intent.getSerializableExtra("calendarFirstTime") as Calendar?
        choosedColor = intent.getIntExtra("color", 0)
        listOfDate = ArrayList()

        for (i in firstDate!!.get(Calendar.DAY_OF_MONTH) .. lastDate?.get(Calendar.DAY_OF_MONTH)!!) {
            val date: Calendar = Calendar.getInstance()
            date.set(Calendar.DAY_OF_MONTH,i)
            listOfDate!!.add(date)
        }
        val eventList : ArrayList<EventDay> = ArrayList()
        for(calendar in listOfDate!!){
            eventList.add(EventDay(calendar, R.drawable.ic_baseline_arrow_forward_ios_24,choosedColor!!))
        }
        eventCalendar!!.setEvents(eventList)
    }
}
