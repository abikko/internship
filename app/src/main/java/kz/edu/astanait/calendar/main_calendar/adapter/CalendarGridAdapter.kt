package kz.edu.astanait.calendar.main_calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import androidx.recyclerview.widget.RecyclerView
import kz.edu.astanait.calendar.R

class CalendarGridAdapter(val dates: ArrayList<Int>) : BaseAdapter() {

    override fun getCount(): Int = dates.size

    override fun getItem(position: Int): Any = dates[position]

    override fun getItemId(position: Int): Long = dates[position].toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View =
        LayoutInflater.from(parent!!.context).inflate(R.layout.date_dialog, null)
}