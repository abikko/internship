package kz.edu.astanait.calendar.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.time.Month
import java.time.Year
import kotlin.math.absoluteValue

class MainActivityViewModel : ViewModel() {
    val mYear: MutableLiveData<Int> = MutableLiveData<Int>()
    val mMonth: MutableLiveData<Int> = MutableLiveData<Int>()
    val mDay: MutableLiveData<Int> = MutableLiveData<Int>()
    val mEndYear: MutableLiveData<Int> = MutableLiveData<Int>()
    val mEndMonth: MutableLiveData<Int> = MutableLiveData<Int>()
    val mEndDay: MutableLiveData<Int> = MutableLiveData<Int>()

    fun sendDate(year: Int, month: Int, day: Int) {
        mYear.value = year
        mMonth.value = month
        mDay.value = day
    }

    fun sendDate(
        startYear: Int,
        startMonth: Int,
        startDay: Int,
        endYear: Int,
        endMonth: Int,
        endDay: Int,
    ) {
        mYear.value = startYear
        mMonth.value = startMonth
        mDay.value = startDay
        mEndYear.value = endYear
        mEndMonth.value = endMonth
        mEndDay.value = endDay
    }
}