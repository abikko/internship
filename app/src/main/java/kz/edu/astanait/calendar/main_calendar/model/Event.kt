package kz.edu.astanait.calendar.main_calendar.model

class Event(
    val eventName : String,
    val year: Int,
    val month: Int,
    val day: Int,
    val choosedColor: Int
) {

}