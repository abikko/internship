package kz.edu.astanait.calendar.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kz.edu.astanait.calendar.R
import kz.edu.astanait.calendar.main_calendar.MultipleDatePickerDialog
import kz.edu.astanait.calendar.ui.written_events.EventsActivity
import ru.slybeaver.slycalendarview.SlyCalendarDialog
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), SlyCalendarDialog.Callback {
    private var rbRed: RadioButton? = null
    private var rbGreen: RadioButton? = null
    private var rbYellow: RadioButton? = null
    private var rbWhiteGreen: RadioButton? = null
    private var rbBlue: RadioButton? = null
    private var rbPurple: RadioButton? = null
    private var rgColors: RadioGroup? = null
    private var btnCreateEvent: Button? = null
    private var btnChooseDate: Button? = null
    private var etEventName: EditText? = null

    @ColorRes
    private var choosedColor: Int = R.color.red_8B
    private var firstTime: Calendar? = null
    private var lastTime: Calendar? = null
    private val TAG = "MainActivity"

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mavm: MainActivityViewModel = MainActivityViewModel()

        rgColors = findViewById(R.id.rgColor)
        rbRed = findViewById(R.id.rbRed)
        rbGreen = findViewById(R.id.rbGreen)
        rbYellow = findViewById(R.id.rbYellow)
        rbWhiteGreen = findViewById(R.id.rbWhiteGreen)
        rbBlue = findViewById(R.id.rbBlue)
        rbPurple = findViewById(R.id.rbPurple)
        btnChooseDate = findViewById(R.id.btnChooseDate)
        btnCreateEvent = findViewById(R.id.btnCreateEvent)
        etEventName = findViewById(R.id.etEnterEvent)

        btnChooseDate!!.setOnClickListener {
            val multipleDatePickerDialog: MultipleDatePickerDialog =
                MultipleDatePickerDialog.newInstance()
            val multipleDatePickerDialogFragment: SlyCalendarDialog = SlyCalendarDialog()
            multipleDatePickerDialogFragment.setSingle(false)
            multipleDatePickerDialogFragment.setCallback(this)
            multipleDatePickerDialogFragment.show(supportFragmentManager, "abyl")
//            multipleDatePickerDialog.show(supportFragmentManager,"Tag")
        }
        btnCreateEvent!!.setOnClickListener {
            when (rgColors!!.checkedRadioButtonId) {
                R.id.rbRed -> {
                    choosedColor = R.color.red_8B
                }
                R.id.rbGreen -> {
                    choosedColor = R.color.green_3C
                }
                R.id.rbYellow -> {
                    choosedColor = R.color.yellow_2D
                }
                R.id.rbWhiteGreen -> {
                    choosedColor = R.color.white_green_38
                }
                R.id.rbBlue -> {
                    choosedColor = R.color.blue_9F
                }
                R.id.rbPurple -> {
                    choosedColor = R.color.purple_A5
                }
            }
            Log.d(TAG, "onCreate LastTime:  " + lastTime)
            val eventsActivity: Intent = Intent(this, EventsActivity::class.java)
            eventsActivity.putExtra("calendarFirstTime",firstTime)
            eventsActivity.putExtra("calendarLastTime",lastTime)
            eventsActivity.putExtra("color",choosedColor)
            startActivity(eventsActivity)
        }
    }

    override fun onCancelled() {
        TODO("Not yet implemented")
    }

    override fun onDataSelected(
        firstDate: Calendar?,
        secondDate: Calendar?,
        hours: Int,
        minutes: Int
    ) {
        if (firstDate != null) {
            if (secondDate == null) {
                firstDate[Calendar.HOUR_OF_DAY] = hours
                firstDate[Calendar.MINUTE] = minutes
                Toast.makeText(
                    this,
                    SimpleDateFormat(getString(R.string.timeFormat), Locale.getDefault()).format(
                        firstDate.time
                    ),
                    Toast.LENGTH_LONG
                ).show()
                firstTime = firstDate
            } else {
                Toast.makeText(
                    this,
                    getString(
                        R.string.period,
                        SimpleDateFormat(
                            getString(R.string.dateFormat),
                            Locale.getDefault()
                        ).format(firstDate.time),
                        SimpleDateFormat(
                            getString(R.string.timeFormat),
                            Locale.getDefault()
                        ).format(secondDate.time)
                    ),
                    Toast.LENGTH_LONG
                ).show()
                firstTime = firstDate
                lastTime = secondDate
            }
        }
    }
}