package kz.edu.astanait.calendar.main_calendar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kz.edu.astanait.calendar.R
import ru.slybeaver.slycalendarview.SlyCalendarDialog
import ru.slybeaver.slycalendarview.SlyCalendarView
import java.text.SimpleDateFormat
import java.util.*


class MultipleDatePickerDialog : DialogFragment(), SlyCalendarDialog.Callback {

    private var cvDatePicker: SlyCalendarView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v: View = inflater.inflate(R.layout.date_dialog, null)

        cvDatePicker = v.findViewById(R.id.cvDatePicker)

        return v
    }


    companion object {
        fun newInstance(): MultipleDatePickerDialog {
            val args = Bundle()
            val fragment = MultipleDatePickerDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCancelled() {
    }

    override fun onDataSelected(
        firstDate: Calendar?,
        secondDate: Calendar?,
        hours: Int,
        minutes: Int
    ) {
        if (firstDate != null) {
            if (secondDate == null) {
                firstDate[Calendar.HOUR_OF_DAY] = hours
                firstDate[Calendar.MINUTE] = minutes
                Toast.makeText(
                    context,
                    SimpleDateFormat(getString(R.string.timeFormat), Locale.getDefault()).format(
                        firstDate.time
                    ),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    context,
                    getString(
                        R.string.period,
                        SimpleDateFormat(
                            getString(R.string.dateFormat),
                            Locale.getDefault()
                        ).format(firstDate.time),
                        SimpleDateFormat(
                            getString(R.string.timeFormat),
                            Locale.getDefault()
                        ).format(secondDate.time)
                    ),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}